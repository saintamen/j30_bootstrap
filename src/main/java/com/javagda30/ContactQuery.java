package com.javagda30;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ContactQuery {
    private String email;
    private String name;
    private String phoneNumber;

    private QueryType queryType; // enum, za chwilę napiszemy

    private String content;
}
