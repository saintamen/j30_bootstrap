package com.javagda30;

public enum QueryType {
    OFFER,
    SUPPORT,
    OTHER
}
