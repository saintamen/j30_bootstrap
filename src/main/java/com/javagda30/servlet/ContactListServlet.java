package com.javagda30.servlet;

import com.javagda30.ContactQuery;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/list")
public class ContactListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<ContactQuery> queries = ContactManager.INSTANCE.getContactQueryList();

        req.setAttribute("contactQueries", queries);
        req.getRequestDispatcher("/list.jsp").forward(req, resp);
    }
}
