package com.javagda30.servlet;

import com.javagda30.ContactQuery;

import java.util.ArrayList;
import java.util.List;

public enum ContactManager {
    INSTANCE;

    private List<ContactQuery> contactQueryList = new ArrayList<>();

    public void add(ContactQuery query){
        contactQueryList.add(query);
    }

    public List<ContactQuery> getContactQueryList() {
        return contactQueryList;
    }
}
