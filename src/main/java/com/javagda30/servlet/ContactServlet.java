package com.javagda30.servlet;

import com.javagda30.ContactQuery;
import com.javagda30.QueryType;

import javax.jws.WebService;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

// adres którego należy użyć by wyołać funkcję servletu
@WebServlet("/submitform")
public class ContactServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Metoda wywołana kiedy zatwierdzimy formularz.");

        String name = req.getParameter("input.name");
        String email = req.getParameter("input.email");
        String phone = req.getParameter("input.phone");
        String type = req.getParameter("input.queryType");
        String content = req.getParameter("input.content");

        // stworzyłem all args constructor by móc użyć tego konstruktora
        ContactQuery query = new ContactQuery(email, name, phone, QueryType.valueOf(type), content);
        ContactManager.INSTANCE.add(query);

        resp.sendRedirect("/list");
    }
}
