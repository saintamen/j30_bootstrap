<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 1/11/20
  Time: 1:03 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Form</title>
    <jsp:include page="header-links.jsp"/>
</head>
<body>
<jsp:include page="header.jsp"/>

<main role="main">
    <jsp:include page="carousel.jsp"/>

    <form action="/submitform" method="post">
        <div class="form-container">
            <h1>Contact form</h1>

            <div class="container">
                <div class="row">
                    <div class="col-4 label-cell">
                        <label for="input.name">Name:</label>
                    </div>
                    <div class="col-8 input-cell">
                        <input id="input.name" name="input.name" class="form-control" placeholder="Name and surname..."
                               type="text"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <label for="input.email">Email:</label>
                    </div>
                    <div class="col-8">
                        <input id="input.email" name="input.email" class="form-control" placeholder="Email address..."
                               type="text"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <label for="input.phone">Phone number:</label>
                    </div>
                    <div class="col-8">
                        <input name="input.phone" id="input.phone" class="form-control" placeholder="Phone number..." type="text"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <label for="input.queryType">Query type:</label>
                    </div>
                    <div class="col-8">
                        <select name="input.queryType" id="input.queryType" class="form-control">
                            <option>OFFER</option>
                            <option>SUPPORT</option>
                            <option>OTHER</option>
                        </select>
                    </div>
                </div>
                <%--            TERAZ PISZĘ TUTAJ !!!!                   --%>
                <div class="row">
                    <div class="col-2">Content:</div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <textarea name="input.content" class="form-control" placeholder="Query content..." rows="8"></textarea>
                    </div>
                </div>
                <div class="row">
                    <input type="reset" class="offset-2 col-3 btn btn-warning" value="Reset">
                    <input type="submit" class="offset-2 col-3 btn btn-primary" value="Submit">
                </div>
            </div>
        </div>
    </form>
</main>

<jsp:include page="footer-scripts.jsp"/>
</body>
</html>
