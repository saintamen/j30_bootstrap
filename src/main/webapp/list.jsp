<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 1/11/20
  Time: 3:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>


<html>
<head>
    <title>List</title>
    <jsp:include page="header-links.jsp"/>
</head>
<body>
<jsp:include page="header.jsp"/>

<main role="main">
    <jsp:include page="carousel.jsp"/>

    <c:forEach items="${requestScope.contactQueries}" var="contact">
        <div>${contact.getName()}</div>
    </c:forEach>
</main>

<jsp:include page="footer-scripts.jsp"/>
</body>
</html>
